import logging
import random
from flask import Flask, render_template
from flask_ask import Ask, statement, question, session, version
from harmony.client import HarmonyClient
import os
import inspect
import time

app = Flask(__name__)
ask = Ask(app, "/")
logging.getLogger("flask_ask").setLevel(logging.DEBUG)
log = logging.getLogger()

TEMPERATURE_UNITS = ['temp', 'temperature']
SOUND_UNITS = ['vol', 'volume', 'music', 'sound', 'noise']
HOT_INDICATORS = ['hot', 'warm', 'melting']
COLD_INDICATORS = ['cold', 'chilly', 'freezing']
AFFIRMITIVE_RESPONSES = ['Yes Master', '', 'Your wish is my command', 'Got it', 'Bazinga!']

device_ids = {
                'shield tv': '28138267',
                'ps4': '32380440',
                'wii u': '22176368',
                'air conditioner': '27899238',
                'television': '21614352',
                'shield': '35293414',
                'sound bar': '23009295'
             }

device_name_variations = {
    'shield tv':
        [
            'shield tv',
            'shield',
            'nvidia shield',
            'nvidia shield tv'
        ],
    'air conditioner':
        [
            'air conditioner',
            'air',
            'ac',
            'a/c',
            'thermostat'
        ],
    'sound bar':
        [
            'sound bar',
            'receiver',
            'speaker',
            'sound'
        ],
    'television':
        [
            'television',
            'tv',
            't.v.',
        ],
    'ps4':
        [
            'ps4',
            'playstation',
            'playstation 4',
            'playstation four',
            'ps four',
            'sony playstation',
            'sony playstation 4',
            'sony playstation four'
        ]
}

@ask.on_session_started
def new_session():
    log.info('new session started')
    print("Alexa Version: {}".format(version))

@ask.launch
def start():
    welcome_msg = render_template('welcome')
    return statement(welcome_msg)\
    .simple_card(title='Wise Hub', content='Examples\n\n{}'.format(open('utterances.txt', 'r').read()))


@ask.intent("GiveCommandIntent", default={'repeat_count': 1}, convert={'repeat_count': int})
def send_command(command, device, repeat_count=1):
    device = search_for_device(device_name_variations, device)
    device_id = device_ids.get(device, device)
    if not command:
        error_msg = render_template('error_no_command', device=device)
        return statement(error_msg)
    if not device:
        error_msg = render_template('error_no_device', command=command)
        return statement(error_msg)

    command = command.replace(' ', '')
    for _ in range(repeat_count):
        if app.debug:
            print('Sending command: {} to device: {}'.format(command, device))
        HARMONY_CLIENT.send_command(device_id, command)
        time.sleep(0.5)

    msg = render_template('affirmitive_response')
    return statement(msg)


@ask.intent("TooMuchIntent")
def too_hot_or_cold(hot_cold):
    if not hot_cold or not (hot_cold in COLD_INDICATORS or hot_cold in HOT_INDICATORS):
        msg = render_template('error_enunciate')
        return question(msg)
    if hot_cold in COLD_INDICATORS:
        set_ac_question = render_template('ask_ac_mode', mode='Energy Saver')
        session.attributes['command'] = 'EnergySaver'

    elif hot_cold in HOT_INDICATORS:
        set_ac_question = render_template('ask_ac_mode', mode='Cool')
        session.attributes['command'] = 'Cool'

    session.attributes['device'] = 'air conditioner'
    return question(set_ac_question)


@ask.intent("AMAZON.YesIntent")
def answer_yes():
    return handle_answer(True)


@ask.intent("AMAZON.NoIntent")
def answer_no():
    msg = render_template('negatory_response')
    return statement(msg)


def handle_answer(answer=None):
    if answer:
        command = session.attributes.get('command')
        device = session.attributes.get('device')
        if command and device:
            return send_command(command, device)
        else:
            response_msg = render_template('error_answer_without_question', answer=answer)
            return statement(response_msg)


@ask.intent("IncrementActionIntent", default={'direction': 'up', 'unit': 'no', 'number': 1}, convert={'number': int})
@ask.intent("DecrementActionIntent", default={'direction': 'down', 'unit': 'no', 'number': 1}, convert={'number': int})
def increment_action(direction, unit, number):
    if unit.lower() in TEMPERATURE_UNITS:
        if direction.lower() == 'up':
            return send_command('TempUp', 'air conditioner', number)
        elif direction.lower() == 'down':
            return send_command('TempDown', 'air conditioner', number)
        else:
            return error_invalid_direction(direction, 'air conditioner')
    elif unit.lower() in SOUND_UNITS:
        if direction.lower() == 'up':
            return send_command('VolumeUp', 'sound bar', number)
        elif direction.lower() == 'down':
            return send_command('VolumeDown', 'sound bar', number)
        else:
            return error_invalid_direction(direction, 'sound bar')
    else:
        debug_msg = render_template('error_command_heard', app_name='wise hub', action=direction, thing=unit)
        log.debug(debug_msg)
        error_msg = render_template('error_generic')
        return statement(error_msg)


@ask.intent("MediaActionIntent", default={'device': 'shield tv'})
def media_action(device, media_command):
    return send_command(media_command, device)


@ask.intent("AMAZON.PauseIntent")
def pause_action():
    command = 'pause'
    device = 'shield tv'
    return send_command(command, device)


@ask.session_ended
def session_ended():
    print('session ended...')
    return "", 200


def error_invalid_direction(direction, device):
    error_msg = render_template('error_invalid_direction', direction=direction, device=device)
    return statement(error_msg)


def search_for_device(synonym_dict, lookup):
    result_set = set()
    try:
        lookup = lookup.lower()
    except AttributeError:
        return lookup
    result = not ['' for key, values in synonym_dict.items() for value in values if lookup in value and result_set.add(key)] and list(result_set)
    if result:
        return result[0]
    else:
        return lookup


def get_harmony_client():
    harmony_ip = os.getenv('harmony_ip')
    harmony_email = os.getenv('harmony_email')
    harmony_pass = os.getenv('harmony_pass')
    if all([harmony_ip, harmony_email, harmony_pass]):
        client = HarmonyClient(harmony_email, harmony_pass, harmony_ip)
    else:
        print('Harmony Credentials not set!')
        exit(1)

    return client

HARMONY_CLIENT = get_harmony_client()

if __name__ == '__main__':
    app.run(debug=True)
